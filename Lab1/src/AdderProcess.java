
public class AdderProcess implements MigratableProcess {
	
	private volatile boolean suspending;  
	private Integer a, b, c; 
	
	public AdderProcess(Integer a, Integer b, Integer c) {
		this.a = a; 
		this.b = b; 
		this.c = c; 
		
	}
	
	public void run() {
		Integer out = a+b+c; 
		System.out.println(out); 
	}
	
	public void suspend() {
		suspending = true; 
		while(suspending); 
	}
		
}
