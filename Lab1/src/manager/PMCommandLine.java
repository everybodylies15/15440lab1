package manager; 

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import processes.MigratableProcess;

public class PMCommandLine extends Thread {

	private String input; 
	
	private static ExecutorService exec; 
	private static ExecutorService execPS; 
    
    // Stores proccesses that are currently being run
    private static ConcurrentHashMap<MigratableProcess, ProcessInfo> processes; 
	
    // The following two lists are ONLY for slave nodes
    // Newly added or recently terminated processes (recently terminated processes list
    // gets updated every time PS is called) -- these lists should be cleared every time
    // a heartbeat happens
    private static List<ProcessInfo> newProcs; 
    private static List<ProcessInfo> deadProcs; 
    
	public PMCommandLine(ConcurrentHashMap<MigratableProcess, ProcessInfo> processes) {
		this.processes = processes; 
		
		// Initialized newly added and dead process lists
		newProcs = Collections.synchronizedList(new ArrayList<ProcessInfo>()); 
		deadProcs = Collections.synchronizedList(new ArrayList<ProcessInfo>()); 
	}
	
	public void run() {
		
	    // Introduction message
	    System.out.println("Hi, welcome to our Process Manager! Here is a list of possible commands:"); 
	    System.out.println("<processName> [arg1] [arg2] ... [argN]"); 
	    System.out.println("ps (prints a list of local running processes and their arguments)"); 
	    System.out.println("quit (exits the ProcessManager)"); 
		
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
	    input = ""; 

		while(true) {
			// Get standard in input
    		System.out.print("==> "); 
    		try {
    			input = br.readLine(); 
    		} catch(IOException ioe) {
    			System.err.println("\nERROR: IOException when reading from Standard In"); 
    		}
    		
    		// Processing command
    		if(input == null || input.trim().equals("")) continue; 
    		String[] inputTokens = input.split(" "); 
    		
    		// Print all running processes
    		if(inputTokens[0].equals("ps")) {
    			printPS();   
    			
    		// Quit ProcessManager
    		} else if(inputTokens[0].equals("quit")) {
    			System.out.println("\nThank you for using this Process Manager! Goodbye!"); 
    			break; 
    			
    		// Run new process
    		} else { 
    			createProcess(inputTokens); 
    		}
    		
		}
	}
	
	public String getInput() {
		return input; 
	}
	
	public static ConcurrentHashMap<MigratableProcess, ProcessInfo> getProcesses() {
		return processes; 
	}
	
	private Object createProcess(String[] inputTokens) { 
		// ALL processes (MigratableProcesses) should be in the "processes" package, which means
		// the String "processes." needs to be concatenated onto all the process class names
		String processClassName = ProcessManager.PROCESS_PACKAGE + inputTokens[0]; 
		
		// Using reflection to invoke a process that the user requests with the given arguments
		try {
			// Class object for the new process
			Class processClass = Class.forName(processClassName); 
			// Constructor object of the process  
			Constructor constructor = processClass.getConstructor(new Class[]{String[].class});    
			// Put together arguments (in String[]) for the constructor call and create a new instance
			// of the process by actually calling the constructor
			String[] arguments = new String[inputTokens.length-1]; 
	    	for(int i=0; i<inputTokens.length-1; i++)  
	    		arguments[i] = new String(inputTokens[i+1]); 
	    	MigratableProcess proc = (MigratableProcess) constructor.newInstance((Object)arguments); 
	    	// Run the process (Java thread)
	    	exec = Executors.newCachedThreadPool(); 
	    	Future<?> future = exec.submit(proc);   
	    	Date d = new Date(); 
	    	String timeStamp = d.toString(); 
	    	
	    	// Put this process instance in a HashMap so that ProcessManager can keep track of it  
			ProcessInfo piTemp = null; 
	    	if(ProcessManager.isSlave())  
	    		piTemp = new ProcessInfo(arguments, inputTokens[0], future, timeStamp, PMSlave.getNodeNum()); 
	    	else 
	    		piTemp = new ProcessInfo(arguments, inputTokens[0], future, timeStamp, PMSlave.getNodeNum()); 
	    	processes.put(proc, piTemp); 
	    	
	    	newProcs.add(piTemp); // Update recently added process list
		} catch(ClassNotFoundException e) {
			System.err.println("\nERROR: Particular process object--" + processClassName + "--cannot be found."); 
			System.err.println("       Note, all processes must be located inside the \"processes\" package.");  
		} catch(NoSuchMethodException e) {
			System.err.println("\nERROR: Process object--" + processClassName + "--is not implemented properly.");  
			System.err.println("       Every process must implement MigratableProcess, be located inside the");   
			System.err.println("       \"processes\" package and have a constructor with one argument--String[],"); 
			System.err.println("       which will be the constructor that is called."); 
		} catch(SecurityException e) {
			e.printStackTrace(); 
			 
		} catch(InstantiationException e) {
			e.printStackTrace(); 
		} catch(IllegalAccessException e) {
			e.printStackTrace(); 
		} catch(IllegalArgumentException e) {
			e.printStackTrace(); 
		} catch(InvocationTargetException e) {
			System.err.println("\nERROR: Exception thrown while running the " + processClassName); 
			System.err.println("       process object:"); 
			Throwable c = e.getCause(); 
			if(c != null) System.err.println("       " + c); 
		} 
		
    	return null; 
    }
	
	private void printPS() {
		// Print node number
		if(ProcessManager.isSlave())
			System.out.println("-----Node " + PMSlave.getNodeNum() + " (slave) Processes-----"); 
		else
			System.out.println("-----Node 0 (master) Processes-----");    
		
		// Iterate though all processes running on current node
		for(MigratableProcess proc: processes.keySet()) {
			String command = proc.getClass().toString().split(ProcessManager.PROCESS_PACKAGE)[1]; 
			String[] arguments = processes.get(proc).getArguments(); 
			for(int i=0; i<arguments.length; i++)   
				command += " " + arguments[i];    
			command += " [" + processes.get(proc).getTimeStamp() + "]";  
			Future<?> future = processes.get(proc).getFuture(); 
			if(future != null && !future.isDone())
				System.out.println(command); 
			else {
				deadProcs.add(processes.get(proc)); // Update recently terminated process list
				System.out.println("added to deadProcs, size of list is " + deadProcs.size());  
				System.out.println("Process \"" + command + "\" was terminated"); 
				// If a process is finished, remove it from the HashMap that is keeping track of processes
				processes.remove(proc); 
			}
		}
		
		// Iterate through all processes running on all other nodes if current node is master
		if(!ProcessManager.isSlave()) {
			// Print all processes from all slave nodes
			List<List<ProcessInfo>> processMasterList = PMClientListener.getProcessMasterList(); 
			int nodeNum = 0; 
			for(List<ProcessInfo> processList: processMasterList) {  
				if(processList == null || nodeNum == 0) {
					++nodeNum; 
					continue; 
				}
				
				System.out.println("-----Node " + nodeNum + " (slave) Processes-----"); 
				for(ProcessInfo pi: processList) {
					String command = pi.getClassName(); 
					String[] arguments = pi.getArguments(); 
					for(int i=0; i<arguments.length; i++)   
						command += " " + arguments[i];    
					command += " [" + pi.getTimeStamp() + "]"; 
					System.out.println(command); 
				}
				
				++nodeNum; 
			}
		}
	}
	
	public static List<ProcessInfo> getNewProcs() {
		if(newProcs.isEmpty()) return null; 
		
		List<ProcessInfo> tempNewProcs = new ArrayList<ProcessInfo>(); 
		// Lock the following actions--make deep copy of newly added process list and clear 
		// the original
		synchronized(newProcs) {
			for(ProcessInfo pi: newProcs) tempNewProcs.add(pi); 
			newProcs.clear(); 
		}
		return tempNewProcs;  
	}
	
	public static List<ProcessInfo> getDeadProcs() {
		if(deadProcs.isEmpty()) return null; 
		
		List<ProcessInfo> tempDeadProcs = new ArrayList<ProcessInfo>(); 
		// Lock the following actions--make deep copy of newly added process list and clear 
		// the original
		synchronized(deadProcs) {
			for(ProcessInfo pi: deadProcs) tempDeadProcs.add(pi); 
			deadProcs.clear(); 
		}
		return tempDeadProcs;  
	}
	
	public static ExecutorService getExec() {
		return exec; 
	}
	
}
