package manager;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import processes.MigratableProcess;

public class PMHeartbeat extends Thread {

	// Keeps track of heartbeat number so master knows that it hasn't missed one
	private long seqNum; 

	// Keeps track of all processes run on this node
	private ConcurrentHashMap<MigratableProcess, ProcessInfo> processes; 

	// Keeps track of number of processes running on this node (the load for this instance of ProcessManager)
	private int load; 
	
	// Serialization output stream for sending heartbeats to server
	ObjectOutputStream outStream; 
	
	public PMHeartbeat(ConcurrentHashMap<MigratableProcess, ProcessInfo> processes, ObjectOutputStream outStream) {
		seqNum = 0; 
		load = 0; 
		this.processes = processes; 
		this.outStream = outStream; 
	}
	
	public void run() {
		// DEBUG
		//System.out.print("HB " + seqNum + " ==> "); 
		++seqNum; 
		load = processes.size(); 
		Map<String, Object> message = new HashMap<String, Object>(); 
		message.put("type", "HB"); 
		message.put("seqNum", new Long(seqNum)); 
		message.put("load", new Integer(load)); 
		// Every 5 heartbeats (25 seconds), slave sends updates to the master for new 
		// processes and terminated processes
		if(seqNum%5 == 0) {
			message.put("newProcs", PMCommandLine.getNewProcs());   
			message.put("deadProcs", PMCommandLine.getDeadProcs()); 
		}
		// Send message serially to master
		try {
			outStream.writeObject(message); 
		} catch (IOException e) {
			System.err.println("\nERROR: Unable to communicate with master--all is lost!"); 
			System.exit(-1); 
		} 
	}
	
}
