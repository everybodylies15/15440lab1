package manager;

import java.io.Serializable;
import java.util.concurrent.Future;

public class ProcessInfo implements Serializable {

	private static final long serialVersionUID = 5000L; 
	
	private String[] arguments; 
	private String className; 
	private transient Future<?> future; 
	private String timeStamp; 
	private int nodeNum;    
	
	public ProcessInfo(String[] arguments, String className, Future<?> future, String timeStamp, int nodeNum) {
		this.arguments = arguments; 
		this.className = className; 
		this.future = future; 
		this.timeStamp = timeStamp;   
		this.nodeNum = nodeNum;   
	}
	
	public String[] getArguments() {
		return arguments;
	}

	public String getClassName() {
		return className;
	}

	public Future<?> getFuture() {
		return future;
	}

	public String getTimeStamp() {
		return timeStamp; 
	}
	
	public boolean equals(ProcessInfo pi) {
		return (this.className == pi.getClassName() && this.arguments == pi.getArguments()
		&& this.timeStamp == pi.getTimeStamp());   
	}
}
