package manager;
import java.lang.InterruptedException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import processes.MigratableProcess;

public class ProcessManager {
	
	// Variables to manage threads needed for ProcessManager's various tasks
    private static ExecutorService exec; 
    private static Future<?> cmdLineFuture;  
    
    // States whether current node is the master or a slave and what the master hostname is
    // (if the current node is a slave)
    private static boolean slave; 
    private String hostname; 
    
    public static final String PROCESS_PACKAGE = "processes.";  
    public static final int portNum = 50; 
    
    // Stores processes that are currently being run
    private ConcurrentHashMap<MigratableProcess, ProcessInfo> processes; 
    
    public static void main (String args[]) {
    	// Determining whether current node is the master or a slave and what the master hostname is
    	boolean slave = false; 
    	String hostname = ""; 
    	if(args.length >= 2 && args[0].equals("-c")) {
    			slave = true; 
    			hostname = args[1]; 
    	}
    	// Error checking ProcessManager command line arguments
    	if((args.length != 0 && args.length != 2) || (args.length != 0 && !args[0].equals("-c"))) {
    		System.err.println("\nWARNING: Some or all of the command line arguments given were not understood and, thus, ignored"); 
    	}
    	
    	// Instantiate ProcessManager (providing it with necessary command line information)
    	new ProcessManager(slave, hostname); 
    }
    
    public ProcessManager(boolean slave, String hostname) {
    	// Determines whether this instance of ProcessManager is a slave or the master
    	ProcessManager.slave = slave;  
    	this.hostname = hostname; 
    	
    	// Initialize data structures to keep track of processes that are currently being run
    	processes = new ConcurrentHashMap<MigratableProcess, ProcessInfo>(); 
    	
    	// Initialize a thread pool to manage tasks required by ProcessManager
    	setupThreads(); 
    	
    	// When user enters "quit" in the command line, this instance of ProcessManager closes
    	waitsToFinish(); 
    }
    
    private void setupThreads() {
    	// Initialize thread to control command prompt that interacts with the user
    	exec = Executors.newCachedThreadPool(); 
    	cmdLineFuture = exec.submit(new PMCommandLine(processes));   
    	// Initialize thread that periodically (every 5 seconds) sends heartbeats IFF current node is a slave
		if(slave) {
    		exec.submit(new PMSlave(processes, hostname)); // This thread will then kick off the heartbeats
		} else {
			// Initialize thread to act as server if this node is the master 
			exec.submit(new PMMaster());  
		}
    }
    
    // Waits for user to quit the command line prompt and ends this 
    private void waitsToFinish() {
    	try {
    		// Once the command line thread ends (user entered "quit")
			cmdLineFuture.get(); 
			
			// If slave, send all of its processes to master before quitting (if master, do nothing)
			sendAllProcesses(); 
			
			// Closes this instance of ProcessManager
			System.exit(0);    
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} 
    }

    // Suspend and serialize ALL processes currently being run and send them all off to
    private void sendAllProcesses() {
    	// TODO: implement this!
    }
    
    public static boolean isSlave() {
    	return slave; 
    }
    
}