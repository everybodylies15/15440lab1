package manager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import processes.MigratableProcess;

public class PMSlave extends Thread {

	// Keeps track of all processes run on this node
	private ConcurrentHashMap<MigratableProcess, ProcessInfo> processes; 

	// Hostname of master node
	String hostname; 
	
	// Socket used to communicate with the master
	private Socket clientSocket; 
		
	// Output stream for heartbeats
	private ObjectInputStream inStream; 
	private ObjectOutputStream outStream; 
	
	// Node number
	private static int nodeNum; 
	
	public PMSlave(ConcurrentHashMap<MigratableProcess, ProcessInfo> processes, String hostname) {
		this.processes = processes; 
		this.hostname = hostname; 
		
		// Open a client socket to communicate with the master node
		try {
			clientSocket = new Socket(InetAddress.getByName(hostname), ProcessManager.portNum); 
			inStream = new ObjectInputStream(clientSocket.getInputStream());  
			outStream = new ObjectOutputStream(clientSocket.getOutputStream()); 
			
			// Get node number
			nodeNum = ((Integer)inStream.readObject()).intValue();   
			System.out.println("Initializing Node " + nodeNum + " (slave)\n");  
			
		} catch (UnknownHostException e) {
			System.err.println("\nERROR: Hostname unrecognized"); 
		} catch (IOException e) {
			System.err.println("\nERROR: Unable to find master node--all is lost!"); 
			System.exit(-1); 
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Schedule heartbeats that are sent to the master node every 5 seconds
		ScheduledExecutorService schExec = Executors.newScheduledThreadPool(8); 
		schExec.scheduleAtFixedRate(new PMHeartbeat(processes, outStream), 0, 5, TimeUnit.SECONDS); 
	}
	
	public void run() {
		//TODO: infinite loop listen for commands from master
		
		ConcurrentHashMap<MigratableProcess, ProcessInfo> processes = PMCommandLine.getProcesses(); 
		
		while(true) {
			try {
				Map<String, Object> inMessage = (Map<String, Object>)inStream.readObject(); 
				
				if(inMessage.containsKey("type") && 
				((String)inMessage.get("type")).equalsIgnoreCase("receive")) {
					ProcessInfo pi = (ProcessInfo)inMessage.get("info"); 
					MigratableProcess mp = (MigratableProcess)inMessage.get("proc");  
					processes.put(mp, pi); 
					Future<?> future = PMCommandLine.getExec().submit(mp); 
					if(future == null) System.out.println("null"); 
					else if(future.isDone()) System.out.println("done?!?!?"); 
					else if(!future.isDone()) System.out.println("not done"); 
					pi.setFuture(future); 
					PMCommandLine.getNewProcs().add(pi); 
				} else if(inMessage.containsKey("type") && 
				((String)inMessage.get("type")).equalsIgnoreCase("migrateToMaster")) {
					Iterator iter = processes.keySet().iterator(); 
					MigratableProcess mp = (MigratableProcess) iter.next(); 
					ProcessInfo pi = processes.get(mp); 
					mp.suspend(); 
					System.out.println("suspended");  
					Map<String, Object> message = new HashMap<String, Object>(); 
					message.put("type", "migrateToMaster"); 
					message.put("proc", mp);  
					message.put("info", pi);  
					outStream.writeObject(message); 
					iter.remove(); 
					PMCommandLine.getDeadProcs().add(pi);   
					System.out.println("migrated to master"); 
				} else if(inMessage.containsKey("type") && 
				((String)inMessage.get("type")).equalsIgnoreCase("migrateToSlave")) {
					Iterator iter = processes.keySet().iterator(); 
					MigratableProcess mp = (MigratableProcess) iter.next(); 
					ProcessInfo pi = processes.get(mp); 
					mp.suspend(); 
					Map<String, Object> message = new HashMap<String, Object>(); 
					message.put("type", "migrateToSlave"); 
					message.put("proc", mp);  
					message.put("info", pi);  
					message.put("slaveNodeNum", inMessage.get("slaveNodeNum")); 
					outStream.writeObject(message); 
					iter.remove(); 
					PMCommandLine.getDeadProcs().add(pi); 
				}
				
			} catch(IOException e) {
				System.err.println("\nERROR: Unable to communicate with master--all is lost!"); 
				e.printStackTrace(); 
				System.exit(-1); 
			} catch(ClassNotFoundException e) {
				System.err.println("\nERROR: Unable to interpret message from master"); 
			}
			

		}
	}

	public static int getNodeNum() {
		return nodeNum; 
	}
	
}
