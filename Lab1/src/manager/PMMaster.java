package manager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PMMaster extends Thread {

	private ServerSocket serverSocket; 
	
	// List of nodes connected to master (master is node 0 by default)
	private static List<Socket> nodes;  
		
	// List of processes running on each node
	private static List<List<ProcessInfo>> processMasterList; 
		
	public PMMaster() {
		PMClientListener.init(); 
	}
	
	public void run() {
		ScheduledExecutorService schExec = Executors.newScheduledThreadPool(8);    
		schExec.scheduleAtFixedRate(new PMMigrateManager(), 0, 7, TimeUnit.SECONDS); 
		
		// Setup a server socket to listen for slaves
		try {
			serverSocket = new ServerSocket(ProcessManager.portNum); 
			while(true) {
				Socket client = serverSocket.accept(); 
				PMClientListener cl= new PMClientListener(client); 
				Thread t = new Thread(cl);  
				t.start(); 
			}
		} catch (IOException e) {
			System.err.println("\nERROR: Could not listen on port: " + ProcessManager.portNum); 
		} 
		
	}
}
