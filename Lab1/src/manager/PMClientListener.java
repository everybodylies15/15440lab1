package manager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

import processes.MigratableProcess;
import processes.NeverDieProcess;

public class PMClientListener implements Runnable {
	
	// Streams for serialization across network
	private ObjectInputStream inStream; 
	private ObjectOutputStream outStream; 
	
	// Client socket that is being listened to on this thread of the server
	private Socket client; 
	
	// List of nodes connected to master (master is node 0 by default)
	private static List<Socket> nodes;  
	
	// Object I/O streams for master to communicate with each node 
	private static List<ObjectInputStream> nodeInputStreams;  
	private static List<ObjectOutputStream> nodeOutputStreams;  
	
	// List of processes running on each node
	private static List<List<ProcessInfo>> processMasterList; 
	
	// List of loads for all nodes
	private static List<Integer> loadList;  
	
	// Node number that this listener is listening to
	private int nodeNum; 
	
	private static List<Map<String, Object>> outMessageList; 
	
	public PMClientListener(Socket client) {
		this.client = client; 
	}
	
	@Override
	public void run() {
		try {
			// Instantiating serialization streams   
			outStream = new ObjectOutputStream(client.getOutputStream()); 
			inStream = new ObjectInputStream(client.getInputStream());  
    		
			// Update list of nodes and inform slave of its node number 
			nodes.add(client); 
			nodeInputStreams.add(inStream); 
			nodeOutputStreams.add(outStream); 
			nodeNum = nodes.size()-1; 
			outStream.writeObject(new Integer(nodeNum)); 
			
			processMasterList.add(null); 
			processMasterList.set(nodeNum, Collections.synchronizedList(new ArrayList<ProcessInfo>())); 
			
			loadList.add(new Integer(-1)); 
			
			// Continue to receive messages from socket forever
			while(true) {
				
				// Check first for pending outMessages
				if(outMessageList != null) {
					for(Map<String, Object> outMessage: outMessageList) {
						if((int)(outMessage.get("slaveNodeNum")) == nodeNum) {
							// Send message to slave to receive and deserialize migratable process    
							outStream.writeObject(outMessage); 
							
						}
					}
				}
				
				// Receive messages from client
				Map<String, Object> message = (Map<String, Object>) inStream.readObject(); 
				// Receiving a heartbeat
				if(message.containsKey("type") && ((String)message.get("type")).equalsIgnoreCase("HB")) {  
					// DEBUG  
					System.out.print("  N" + nodeNum + "_HB" + message.get("seqNum") + "_" + message.get("load") + "  "); 
					
					// Store updated load information
					PMClientListener.loadList.set(nodeNum, (Integer)message.get("load")); 
					
					if(message.containsKey("newProcs") && message.get("newProcs") != null) {
						List<ProcessInfo> newProcs = (List<ProcessInfo>)message.get("newProcs"); 
						// DEBUG
						//System.out.println("Got " + newProcs.size() + " new processes"); 
						for(ProcessInfo pi: newProcs) {
							PMClientListener.getProcessMasterList().get(nodeNum).add(pi); 
						}
						
					}
					// List of processes that have terminated on the slave
					if(message.containsKey("deadProcs") && message.get("deadProcs") != null) {
						List<ProcessInfo> deadProcs = (List<ProcessInfo>)message.get("deadProcs"); 
						// DEBUG
						System.out.println("Got " + deadProcs.size() + " terminated processes"); 
						for(ProcessInfo deadPI: deadProcs) {
							List<ProcessInfo> processList = PMClientListener.getProcessMasterList().get(nodeNum); 
							int i = 0; 
							for(Iterator<ProcessInfo> iter = processList.iterator(); iter.hasNext(); ) {
								ProcessInfo pi = iter.next(); 
								if(pi.equals(deadPI)) {    
									iter.remove(); 
								}
								++i; 
							}
						}
					}
				// Migrate process to master (resume process here)
				} else if(message.containsKey("type") && ((String)message.get("type")).equalsIgnoreCase("migrateToMaster")) {
					ProcessInfo pi = (ProcessInfo)message.get("info"); 
					MigratableProcess mp = (MigratableProcess)message.get("proc");    
					ConcurrentHashMap<MigratableProcess, ProcessInfo> processes = (ConcurrentHashMap<MigratableProcess, ProcessInfo>)PMCommandLine.getProcesses(); 
					processes.put(mp, pi); 
					NeverDieProcess p = (NeverDieProcess)mp; 
					Thread t = new Thread(p); 
					t.start(); 
					/*System.exit(0); 
					if(mp == null) {
						System.out.println("process null??!"); System.exit(-1); 
					}
					Future<?> future = PMCommandLine.getExec().submit(mp); 
					pi.setFuture(future); */
				// Migrate process to different slave (resume process at slave node)  
				} else if(message.containsKey("type") && ((String)message.get("type")).equalsIgnoreCase("migrateToSlave")) {
					ProcessInfo pi = (ProcessInfo)message.get("info"); 
					MigratableProcess mp = (MigratableProcess)message.get("proc");    
					Integer slaveNodeNum = (Integer)message.get("slaveNodeNum"); 
					
					// Send message to slave to receive and deserialize migratable process    
					Map<String, Object> outMessage = new HashMap<String, Object>(); 
					outMessage.put("type", "receive"); 
					outMessage.put("proc", mp);  
					outMessage.put("info", pi); 
					outMessage.put("slaveNodeNum", slaveNodeNum); 
					outMessageList.add(outMessage); 
				} 
			}
		} catch (IOException e) {
			System.err.println("ERROR: Node " + nodeNum + " has abruptly disconnected"); 
			PMClientListener.processMasterList.get(nodeNum).clear(); 
		} catch (ClassNotFoundException e) {
	        System.err.println("ERROR: Unable to interpret message from slave"); 
		}
		
		finally {
			try {
				outStream.close();
				inStream.close();
				client.close();
			} catch (IOException e) {
				System.err.println("ERROR: IOException while trying to close connection with client"); 
				e.printStackTrace(); 
			}
		}
	}
	
	// Initialize static list of nodes and static list of all processes
	public static void init() {
		// Initialize list of sockets
		nodes = Collections.synchronizedList(new ArrayList<Socket>()); 
		nodes.add(null);   // "Node 0" should be assigned to the master (this node)
		
		nodeInputStreams = Collections.synchronizedList(new ArrayList<ObjectInputStream>()); 
		nodeOutputStreams = Collections.synchronizedList(new ArrayList<ObjectOutputStream>()); 
		nodeInputStreams.add(null); 
		nodeOutputStreams.add(null); 
		
		processMasterList = Collections.synchronizedList(new ArrayList<List<ProcessInfo>>()); 
		processMasterList.add(Collections.synchronizedList(new ArrayList<ProcessInfo>())); 

		loadList = Collections.synchronizedList(new ArrayList<Integer>()); 
		loadList.add(new Integer(-1)); 
		
		outMessageList = Collections.synchronizedList(new ArrayList<Map<String, Object>>()); 
	}
	
	public static List<Socket> getNodes() {
		return nodes;   
	}
	
	public static List<ObjectInputStream> getNodeInputStreams() {
		return nodeInputStreams;   
	}

	public static List<ObjectOutputStream> getNodeOutputStreams() {
		return nodeOutputStreams;   
	}
	
	public static List<List<ProcessInfo>> getProcessMasterList() {
		return processMasterList; 
	}
	
	public static List<Integer> getLoadList() {
		return loadList; 
	}
	
}
