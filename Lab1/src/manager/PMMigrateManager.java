package manager;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import processes.MigratableProcess;

public class PMMigrateManager extends Thread {

	public PMMigrateManager() {
		
	}
	
	public void run() {
		System.out.println("migrate manager here"); 
		
		List<Integer> loadList = PMClientListener.getLoadList(); 
		 // Update load for master (ONLY HAPPENS HERE, since it's only needed here)
		ConcurrentHashMap<MigratableProcess, ProcessInfo> processes = (ConcurrentHashMap<MigratableProcess, ProcessInfo>)PMCommandLine.getProcesses(); 
		loadList.set(0, processes.keySet().size());  
		
		// Decide which process to migrate based on loads of all processes
		int max = 0; 
		int min = Integer.MAX_VALUE-1; 
		int maxNode = -1; 
		int minNode = -1; 
		int nodeNum = 0; 
		for(Integer load: loadList) {
			if(load == -1) continue; 
			if(load >= max) {
				max = load; 
				maxNode = nodeNum; 
			} 
			if(load <= min) {
				min = load; 
				minNode = nodeNum; 
			}
			++nodeNum; 
		}
		if(minNode<0 || maxNode<0 || minNode==maxNode) return;  
		
		List<ObjectOutputStream> outStreamList = PMClientListener.getNodeOutputStreams(); 
		
		try {
			if(maxNode == 0) { // Migrate to slave
				Iterator iter = processes.keySet().iterator(); 
				MigratableProcess mp = (MigratableProcess) iter.next(); 
				ProcessInfo pi = processes.get(mp); 
				// Suspend process
				mp.suspend(); 
				
				// Send message to slave to receive and deserialize migratable process    
				Map<String, Object> message = new HashMap<String, Object>(); 
				message.put("type", "receive"); 
				message.put("proc", mp);  
				message.put("info", pi);  
				outStreamList.get(minNode).writeObject(message); 
				
				iter.remove(); 
			} else if(minNode == 0) { // Migrate to master
				// Send message to slave to serialize migratable process and send to master
				Map<String, Object> message = new HashMap<String, Object>(); 
				message.put("type", "migrateToMaster"); 
				outStreamList.get(maxNode).writeObject(message); 
				System.out.println("sent request"); 
			}
			
			for(ObjectOutputStream outStream: outStreamList) {
				// Send message to slave to serialize migratable process and send to master to send to slave
				Map<String, Object> message = new HashMap<String, Object>(); 
				message.put("type", "migrateToSlave"); 
				message.put("slaveNodeNum", minNode); 
				outStreamList.get(maxNode).writeObject(message); 
			}
		} catch(IOException e) {
			System.err.println("ERROR: Unable to communicate with slave to migrate process"); 
		}
	}
	
}
