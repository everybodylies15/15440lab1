import java.io.PrintStream;
import java.io.EOFException;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.Thread;
import java.lang.InterruptedException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.text.html.HTMLDocument.Iterator;

public class ProcessManager {
    
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main (String args[]) {
    	// Introduction message
    	System.out.println("Hi, welcome to our Process Manager! Here is a list of possible commands:"); 
    	System.out.println("<processName> [arg1] [arg2] ... [argN]"); 
    	System.out.println("ps (prints a list of local running processes and their arguements)"); 
    	System.out.println("quit (exits the ProcessManager)"); 
    	
    	ProcessManager pm = new ProcessManager(); 
    	
    	// Standard input command prompt
    	String input = ""; 
    	while(true) {
    		// Get standard in input
    		System.out.print("==> "); 
    		try {
    			input = br.readLine(); 
    		} catch(IOException ioe) {
    			System.err.println("IO Exception when reading from Standard In"); 
    		}
    		
    		// Processing command
    		StringTokenizer st = new StringTokenizer(input, " "); 
    		ArrayList<String> inputTokens = new ArrayList<String>(); 
    		while(st.hasMoreTokens()) { 
    			inputTokens.add(st.nextToken()); 
    		}
    		
    		// Print all running processes
    		if(inputTokens.get(0).equals("ps")) {
    			
    		// Quit ProcessManager
    		} else if(inputTokens.get(0).equals("quit")) {
    			break; 
    			
    		// Run new process
    		} else { 
    			pm.createProcess(inputTokens); 
    		}
    	}
    }
    
    public ProcessManager() {
    	
    }
    
    private Object createProcess(ArrayList<String> inputTokens) {
    	try {
    		Class processClass = Class.forName(inputTokens.get(0)); 
    		System.out.println("hello" + inputTokens.get(0)); 
    		Constructor constructor = processClass.getConstructor(new Class[]{Integer.class, Integer.class, Integer.class});   
    		Object[] arguments = new Object[inputTokens.size()-1]; 
	    	for(int i=0; i<inputTokens.size()-1; i++)
	    		arguments[i] = inputTokens.get(i+1); 
	    	MigratableProcess proc = (MigratableProcess) constructor.newInstance(arguments); 
	    	
	    } catch(Exception e) {
	    	System.out.println("Exception " + e); 
	    }
    	return null; 
    }
}