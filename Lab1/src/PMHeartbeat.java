package manager;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import processes.MigratableProcess;

public class PMHeartbeat extends Thread {

	// Keeps track of heartbeat number so master knows that it hasn't missed one
	private long seqNum; 

	// Keeps track of all processes run on this node
	private ConcurrentHashMap<MigratableProcess, ProcessInfo> processes; 

	// Keeps track of number of processes running on this node (the load for this instance of ProcessManager)
	private int load; 
	
	// Serialization output stream for sending heartbeats to server
	ObjectOutputStream outStream; 
	
	public PMHeartbeat(ConcurrentHashMap<MigratableProcess, ProcessInfo> processes, ObjectOutputStream outStream) {
		seqNum = 0; 
		load = 0; 
		this.processes = processes; 
		this.outStream = outStream; 
	}
	
	public void run() {
		// TODO: Heartbeat code (maybe constructor should take in a socket variable?)
		// DEBUG
		System.out.print("HB " + seqNum + " ==> "); 
		++seqNum; 
		load = processes.size(); 
		Map<String, Object> message = new HashMap<String, Object>(); 
		message.put("type", "HB"); 
		message.put("seqNum", new Long(seqNum)); 
		message.put("load", new Integer(load)); 
		try {
			outStream.writeObject(message); 
		} catch (IOException e) {
			System.err.println("\nERROR: IOException while sending heartbeat"); 
			e.printStackTrace();
		} 
	}
	
}
