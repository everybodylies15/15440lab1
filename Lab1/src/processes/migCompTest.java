package processes;

public class migCompTest {

	private static MigratableCompress testCompress;
	private static MigratableDecompress testDecompress;

	public static void main (String[] args) {
		try {
			System.out.println("Creating MigratableCompress object (" + args[0] + " " + args[1] + ")");
			MigratableCompress testCompress = new MigratableCompress (args);
			
			System.out.println("MigratableCompress.compress()");
			testCompress.compress();
			
			System.out.println("Creating MigratableDecompress object (" + args[1] + " " + args[2]+ ")");
			MigratableDecompress testDecompress = new MigratableDecompress (args);
			
			System.out.println("MigratableDecompress.decompress()");
			testDecompress.decompress();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}