//import TransactionalFileOutputStream;
//import TransactionalFileInputStream;

public class TransactionalFileTester {
	public static void main (String[] args) {
		System.out.println("***********************************************");
		System.out.println("           Testing Input Stream");
		System.out.println("***********************************************");
		try {
			byte[] chars = new byte[2048];
			TransactionalFileInputStream in = new TransactionalFileInputStream("TransactionalFileTester.java");
			//TransactionalFileOutputStream out =  new TransactionalFileOutputStream();

			in.read(chars);
			int x = 0;
			while ( x < 2047) {
				System.out.print((char)chars[x]);
				x++;
			}
		}
		catch (Exception e) {
			System.out.println("TransactionalFileTester thew exception:  ");
			e.printStackTrace();
		}
		System.out.println("***********************************************");
		System.out.println("           Testing Output Stream");
		System.out.println("***********************************************");
		try {
			byte[] chars = new byte[2048];
			TransactionalFileInputStream in2 = new TransactionalFileInputStream("TransactionalFileTester.java");
			TransactionalFileOutputStream out =  new TransactionalFileOutputStream("TestOutput.txt");

			in2.read(chars);
			int x = 0;
			while ( x < 2047) {
				out.write((char)chars[x]);
				x++;
			}
		}
		catch (Exception e) {
			System.out.println("TransactionalFileTester thew exception:  ");
			e.printStackTrace();
		}


	}
}