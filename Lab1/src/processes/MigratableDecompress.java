package processes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class MigratableDecompress implements MigratableProcess {

	private volatile boolean              suspending;
	private String                        sourceFile,  destinationFile;
	private AtomicLong                    sourceIndex;
	private TransactionalFileInputStream  input;
	private TransactionalFileOutputStream output;
	private GZIPInputStream 			  GZIPIn;
	
	public MigratableDecompress(String [] args) throws FileNotFoundException, IOException {
		this.input = new TransactionalFileInputStream(args[1]);
		this.output = new TransactionalFileOutputStream(args[2]);
		this.sourceIndex = new AtomicLong(0);
	}
	public void suspend() {
		suspending = true;
		while(suspending);
	}
	
	public void run(){
		/*try{
			GZIPOutputStream GZIPOut = new GZIPOutputStream(output);
			while (input.available() > 0) {
					//implement input file counter and skip to mark
					GZIPOut.write(input.read());
					System.out.println(this.input.available());
			}
			System.out.println("RUN: calling GZIPOut.close() in decompress");
			GZIPOut.close();
		} catch (Exception e) {
			System.out.println("Exception" + e);
		}
		suspending = false;
		*/
	}
	
	public void run(){
		try{
			GZIPInputStream GZIPIn = new GZIPInputStream(input);
			while (GZIPIn.available() > 0) {
					output.write(GZIPIn.read());
			}
			//System.out.println(".close() in decompress");
			GZIPIn.close();
			output.close();
			
		} catch (Exception e) {
			System.out.println("Exception" + e);
		}
		suspending = false;
	}
	
	public String toString(){
		String returnValue = "";
		return returnValue;
	}
}