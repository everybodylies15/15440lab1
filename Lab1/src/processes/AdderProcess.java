package processes;


public class AdderProcess implements MigratableProcess {
	
	private volatile boolean suspending;  
	private Integer a, b, c; 
	
	private static final long serialVersionUID = 125L; 
	
	// Adds three numbers together
	public AdderProcess(String[] args) {  
		this.a = Integer.parseInt(args[0]); 
		this.b = Integer.parseInt(args[1]); 
		this.c = Integer.parseInt(args[2]); 
	}
	
	// This process is too short to suspend (was only written as a sanity check)
	public void run() {
		Integer out = a+b+c; 
		System.out.println("AdderProcess("+a+", "+b+", "+c+"): Sum of arguments is " + out); 
	}
	
	public void suspend() {
		suspending = true; 
		while(suspending); 
	}
		
}
