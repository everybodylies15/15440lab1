package processes;

public class NeverDieProcess implements MigratableProcess {
	
	private volatile boolean suspending;  
	
	private static final long serialVersionUID = 200L; 
	
	// Does nothing
	public NeverDieProcess(String[] args) {}
	
	// Runs forever but is suspendable
	public void run() {
		while(!suspending); 
		suspending = false; 
	}
	
	public void suspend() {
		suspending = true; 
		while(suspending); 
	}
		
}
