package processes;

public class SlowAccumulationProcess implements MigratableProcess {
		
	private volatile boolean suspending;  
	private int limit, sleepTime; 
	private long out; 
	private int i;   
	
	private static final long serialVersionUID = 500L; 
	
	public SlowAccumulationProcess(String[] args) {  
		limit = (int)Integer.parseInt(args[0]); // Number to accumulate to
		sleepTime = (int)Integer.parseInt(args[1]); // Sleep time between accumulations
		if(limit < 0 || sleepTime < 0) {
			throw new IllegalArgumentException("Both arguments must be non-negative ints"); 
		}
		//System.out.println("This process will add all numbers between 1 and " + limit 
		//		+ ",sleeping for " + sleepTime + " msecs between adds");  
		out = 0; 
		i = 0; 
	}
	
	public void run() {
		while(!suspending) {
			if(i > limit) {
				// DEBUG
				System.out.println("SlowAccumulationProcess("+limit+", "+sleepTime+"): Final accumulation is " + out); 
				//
				break; 
			}
			
			out = (long)i + out; 
			try {
				Thread.sleep((long)sleepTime);  
			} catch (InterruptedException e) {
				System.err.println("Interrupt!"); 
			} 
			
			// DEBUG
			//System.out.println("i increment " + i); 
			++i;  
		}
		
		suspending = false; 
	}
	
	public void suspend() {
		suspending = true; 
		while(suspending); 
	}
}
