package processes;

import java.io.*; 

public class SerializeProcessTester {

	public static void main(String[] args) {
		// Run thread
		MigratableProcess proc = new SlowAccumulationProcess(new String[]{"10", "1000"}); 
		Thread t = new Thread(proc); 
		t.start(); 
		System.out.println("Run process (should take 10 seconds to run): SlowAccumulatorProcess(10000, 1)"); 
		
		// Sleep for 2.5 seconds
		try { 
			Thread.sleep(2500); 
		} catch (InterruptedException e) {
			e.printStackTrace(); 
		} 
		System.out.println("Sleep for 2.5 seconds"); 
		
		// Suspend process
		proc.suspend(); 
		System.out.println("Suspend process"); 
		
		// Serialize process
		try {
			ObjectOutput s = new ObjectOutputStream(new FileOutputStream("testserialize.dat")); 
			s.writeObject(proc); 
			s.flush();  
		} catch (IOException e) {
			e.printStackTrace(); 
		} 
		System.out.println("Serialize process"); 
		
		// Deserialize process
		MigratableProcess proc2 = null; 
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("testserialize.dat"));
			proc2 = (MigratableProcess)in.readObject(); 
		} catch (IOException e) {
			e.printStackTrace(); 
		} catch (ClassNotFoundException e) { 
			e.printStackTrace(); 
		}
		System.out.println("Deserialize process"); 
		
		// Resume process
		Thread t2 = new Thread(proc2); 
		t2.start(); 
		System.out.println("Resume process"); 

	}
	
}
