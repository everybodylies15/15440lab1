//800 verizon
//high speed internet tech support
//Tracy 855 334 6954

package processes;

import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.concurrent.atomic.AtomicLong;

public class MigratableCompress implements MigratableProcess {
	
	private volatile boolean              suspending;
	private AtomicLong                    sourceIndex;
	private TransactionalFileInputStream  input;
	private TransactionalFileOutputStream output;
	private GZIPOutputStream 			  GZIPOut;
	
	public MigratableCompress(String [] args) throws FileNotFoundException, IOException {
		this.input = new TransactionalFileInputStream(args[0]);
		this.output = new TransactionalFileOutputStream(args[1]);
		this.sourceIndex = new AtomicLong();
	}
	
	public void suspend() {
		suspending = true;
		while(suspending);
	}
	
	/*public void run(){
		try{
			System.out.println("In MigratableCompress.run()");
			GZIPOutputStream GZIPOut = new GZIPOutputStream(output);
			while (!suspending) {
				if (this.input.available() >= 0) {
					this.input.skip(this.sourceIndex.getAndIncrement());
					GZIPOut.write(this.input.read());
				}
			}
			GZIPOut.close();
		} catch (Exception e) {
			System.out.println("Exception" + e);
		} 
		suspending = false;
	}*/

	public void run(){
		try{
			GZIPOutputStream GZIPOut = new GZIPOutputStream(output);
			while (this.input.available() > 0) {
				GZIPOut.write(this.input.read());
			}
			//System.out.println(".close() in compress");
			GZIPOut.close();
			input.close();
		} catch (Exception e) {
			System.out.println("Exception" + e);
		} 
	}
	public String toString(){
		String returnValue = "";
		return returnValue;
	}
}