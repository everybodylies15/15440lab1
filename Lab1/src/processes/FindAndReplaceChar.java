package processes;

import java.io.FileNotFoundException;
import java.io.IOException;

public class FindAndReplaceChar implements MigratableProcess {
	
	private volatile boolean				suspending;
	private TransactionalFileInputStream	input;
	private TransactionalFileOutputStream 	output;
	private String 							CurrentLine;
	private char 							current, find, replace;

	//input, output, find, replace
	public FindAndReplaceChar(String [] args) throws FileNotFoundException, IOException {
		input = new TransactionalFileInputStream(args[0]);
		output = new TransactionalFileOutputStream(args[1]);
		
		if (args[2].length() == 1 && args[3].length() == 1){
			find = args[2].charAt(0);
			replace = args[3].charAt(0);
		}
	}
	
	public void suspend() {
		
	}
	
	public void run() {
		try{
			while (input.available() > 0){
				current = (char)input.read();
				if (current == find){
					output.write(replace);
				}
				else {
					output.write(current);
				}
			}
		} catch (Exception e) {
			System.out.println("Exception" + e);
		} 
	}
}