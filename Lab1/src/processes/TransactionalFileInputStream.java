package processes;

import java.io.FileInputStream;
import java.io.Serializable;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.concurrent.atomic.AtomicLong;

public class TransactionalFileInputStream extends FileInputStream implements Serializable  {
	AtomicLong index = new AtomicLong();

	public TransactionalFileInputStream(String file) throws FileNotFoundException  {
		super(file);
		this.index.set(0);
	}
	public TransactionalFileInputStream(String file, long l) throws FileNotFoundException  {
		super(file);
		this.index.set(l);
	}
	public int read(byte[] b, int length) throws IOException  {
		this.index.addAndGet((long)length);
		return this.read(b, index.intValue(), length);
	}
	/*public long skip(long length) throws IOException {
		System.out.println("Skip Length: " + length);
		return this.skip(length);
	}*/
	public void suspend() throws IOException{
		this.close();
	}
}