package processes;

import java.io.FileOutputStream;
import java.io.Serializable;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.concurrent.atomic.AtomicLong;

public class TransactionalFileOutputStream extends FileOutputStream implements Serializable  {
	AtomicLong index = new AtomicLong();

	public TransactionalFileOutputStream(String file) throws FileNotFoundException {
		super(file);
		this.index.set(0);
	}
	public TransactionalFileOutputStream(String file, long l) throws FileNotFoundException {
		super(file);
		this.index.set(l);
	}

	public void write(byte[] b, int length) throws IOException  {
		this.index.addAndGet(length);
		this.write(b, index.intValue(), length);
	}

	public long skip(long length) throws IOException {
		this.index.addAndGet((long)length);
		return this.skip(length);
	}

	public void suspend() throws IOException {
		this.flush();
		this.close();
	}
}
